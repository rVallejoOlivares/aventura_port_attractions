<?php


namespace App\Tests;


use App\Entity\Ride;
use App\Entity\RollerCoaster;
use App\Entity\Wagon;
use PHPUnit\Framework\TestCase;

class RollerCoasterTest extends TestCase
{
    public function testCreationOk()
    {
        $wagons = [$this->createMock(Wagon::class)];

        $obj = new RollerCoaster(
            'aName',
            $wagons,
            Ride::STATUS_STARTED,
            98,
            50,
            100,
            34,
            'aMaterial'
        );

        $this->assertSame('aName', $obj->Name());
        $this->assertSame($wagons, $obj->Wagons());
        $this->assertSame(Ride::STATUS_STARTED, $obj->Status());
        $this->assertSame(98, $obj->NumLoops());
        $this->assertSame(50, $obj->MaxSpeed());
        $this->assertSame(100, $obj->InitialDropDistance());
        $this->assertSame(34, $obj->MinWagons());
        $this->assertSame('aMaterial', $obj->Material());
    }

    public function testInvalidStatus()
    {
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage('Invalid status');

        $wagons = [$this->createMock(Wagon::class)];

        $obj = new RollerCoaster(
            'aName',
            $wagons,
            'INVALIDSTATUS',
            98,
            50,
            100,
            34,
            'aMaterial'
        );
    }

    public function testWagonsEmpty()
    {
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage('Invalid wagons, it cant be empty');

        $obj = new RollerCoaster(
            'aName',
            [],
            Ride::STATUS_STOPPED,
            98,
            50,
            100,
            34,
            'aMaterial'
        );
    }

    public function testNumLoopsNegative()
    {
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage('Invalid number of loops, it cant be negative');

        $wagons = [$this->createMock(Wagon::class)];

        $obj = new RollerCoaster(
            'aName',
            $wagons,
            Ride::STATUS_STOPPED,
            -2,
            50,
            100,
            23,
            'aMaterial'
        );
    }

    public function testMaxSpeedNegative()
    {
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage('Invalid max speed, it cant be negative or zero');

        $wagons = [$this->createMock(Wagon::class)];

        $obj = new RollerCoaster(
            'aName',
            $wagons,
            Ride::STATUS_STOPPED,
            23,
            0,
            100,
            23,
            'aMaterial'
        );
    }

    public function testInitialDropDistanceNegative()
    {
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage('Invalid initial drop distance, it cant be negative');

        $wagons = [$this->createMock(Wagon::class)];

        $obj = new RollerCoaster(
            'aName',
            $wagons,
            Ride::STATUS_STOPPED,
            23,
            45,
            -10,
            23,
            'aMaterial'
        );
    }

    public function testMinimumWagonsNegative()
    {
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage('Invalid minimum of wagons, it cant be negative or zero');

        $wagons = [$this->createMock(Wagon::class)];

        $obj = new RollerCoaster(
            'aName',
            $wagons,
            Ride::STATUS_STOPPED,
            23,
            45,
            45,
            0,
            'aMaterial'
        );
    }

    public function testRemoveWagonOk()
    {
        $wagons = [$this->createMock(Wagon::class),$this->createMock(Wagon::class)];

        $obj = new RollerCoaster(
            'aName',
            $wagons,
            Ride::STATUS_STOPPED,
            23,
            45,
            45,
            1,
            'aMaterial'
        );

        $obj->removeWagon();

        $this->assertSame(1, count($obj->Wagons()));

    }

    public function testAddWagonOk()
    {
        $wagons = [$this->createMock(Wagon::class)];

        $obj = new RollerCoaster(
            'aName',
            $wagons,
            Ride::STATUS_STOPPED,
            23,
            45,
            45,
            1,
            'aMaterial'
        );

        $obj->addWagon();

        $this->assertSame(2, count($obj->Wagons()));
    }


    //NOT A REAL TEST
    public function testDoLapOk()
    {
        $wagons = [$this->createMock(Wagon::class)];

        $obj = new RollerCoaster(
            'aName',
            $wagons,
            Ride::STATUS_STARTED,
            23,
            45,
            45,
            1,
            'aMaterial'
        );

        $obj->doLap();
    }
    //NOT A REAL TEST
}