<?php


namespace App\Tests;


use App\Entity\BigDrop;
use App\Entity\Ride;
use App\Entity\Wagon;
use PHPUnit\Framework\TestCase;

class BigDropTest extends TestCase
{
    public function testCreationOk()
    {
        $wagons = [$this->createMock(Wagon::class)];

        $obj = new BigDrop(
            'aName',
            $wagons,
            Ride::STATUS_STOPPED,
            23,
            45
        );

        $this->assertSame('aName', $obj->Name());
        $this->assertSame($wagons, $obj->Wagons());
        $this->assertSame(Ride::STATUS_STOPPED, $obj->Status());
        $this->assertSame(23, $obj->DropDistance());
        $this->assertSame(45, $obj->MaxSpeed());
    }

    public function testInvalidStatus()
    {
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage('Invalid status');

        $wagons = [$this->createMock(Wagon::class)];

        $obj = new BigDrop(
            'aName',
            $wagons,
            'Invalid',
            23,
            45
        );
    }

    public function testWagonsEmpty()
    {
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage('Invalid wagons, it cant be empty');

        $obj = new BigDrop(
            'aName',
            [],
            'Invalid',
            23,
            45
        );
    }

    public function testDropDistanceNegative()
    {
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage('Invalid drop distance, it cant be negative');

        $wagons = [$this->createMock(Wagon::class)];

        $obj = new BigDrop(
            'aName',
            $wagons,
            Ride::STATUS_STOPPED,
            -23,
            45
        );
    }

    public function testMaxSpeed()
    {
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage('Invalid max speed, it cant be negative');

        $wagons = [$this->createMock(Wagon::class)];

        $obj = new BigDrop(
            'aName',
            $wagons,
            Ride::STATUS_STOPPED,
            23,
            -2
        );
    }

}