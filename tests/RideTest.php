<?php


namespace App\Tests;


use App\Entity\Ride;
use App\Entity\Wagon;
use PHPUnit\Framework\TestCase;

class RideTest extends TestCase
{
    public function testTotalSeatsOk()
    {
        $wagon = new Wagon(5, "Troncs");

        $wagons = [$wagon,$wagon];

        $anonymous = new class('aname',$wagons,Ride::STATUS_STOPPED) extends Ride {
            public function doLap() { return ''; }
        };

        $this->assertSame(10,$anonymous->getTotalSeats());
    }

    public function testFillRIdeOk()
    {
        $wagon = new Wagon(23, "Troncs");

        $wagons = [$wagon];

        $anonymous = new class('aName',$wagons,Ride::STATUS_STOPPED) extends Ride {
            public function doLap() { return ''; }
        };

        //NOT OPTIMUM
        $rand = $anonymous->fillRide();
        $this->assertGreaterThanOrEqual(10, $rand);
        $this->assertLessThanOrEqual(23, $rand);
        //NOT OPTIMUM
    }

    public function testStartUpOk()
    {
        $wagons = [$this->createMock(Wagon::class)];

        $obj = new class('aName', $wagons, Ride::STATUS_STOPPED) extends Ride {
            public function doLap() { return ''; }
        };

        $obj->startUp();

        $this->assertSame(Ride::STATUS_STARTED, $obj->Status());
    }

    public function testShutDownOk()
    {
        $wagons = [$this->createMock(Wagon::class)];

        $obj = new class('aName', $wagons, Ride::STATUS_STARTED) extends Ride {
            public function doLap() { return ''; }
        };

        $obj->shutDown();

        $this->assertSame(Ride::STATUS_STOPPED, $obj->Status());
    }
}