<?php


namespace App\Tests;


use App\Entity\BigDrop;
use App\Entity\Ride;
use App\Entity\Wagon;
use App\Entity\WaterRide;
use PHPUnit\Framework\TestCase;

class WaterRideTest extends TestCase
{
    public function testCreationOk()
    {
        $wagons = [$this->createMock(Wagon::class)];

        $obj = new WaterRide(
            'aName',
            $wagons,
            Ride::STATUS_STOPPED,
            18,
            23,
            45
        );

        $this->assertSame('aName', $obj->Name());
        $this->assertSame($wagons, $obj->Wagons());
        $this->assertSame(Ride::STATUS_STOPPED, $obj->Status());
        $this->assertSame(18, $obj->Duration());
        $this->assertSame(23, $obj->MinWagons());
        $this->assertSame(45, $obj->Falls());
    }

    public function testInvalidStatus()
    {
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage('Invalid status');

        $wagons = [$this->createMock(Wagon::class)];

        $obj = new WaterRide(
            'aName',
            $wagons,
            'invalid',
            18,
            23,
            45
        );
    }

    public function testWagonsEmpty()
    {
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage('Invalid wagons, it cant be empty');

        $obj = new WaterRide(
            'aName',
            [],
            Ride::STATUS_STOPPED,
            18,
            23,
            45
        );
    }

    public function testDurationNegative()
    {
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage('Invalid duration, it cant be negative or zero');

        $wagons = [$this->createMock(Wagon::class)];

        $obj = new WaterRide(
            'aName',
            $wagons,
            Ride::STATUS_STOPPED,
            0,
            23,
            45
        );
    }

    public function testMinWagonsNegative()
    {
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage('Invalid minimum of wagons, it cant be negative or zero');

        $wagons = [$this->createMock(Wagon::class)];

        $obj = new WaterRide(
            'aName',
            $wagons,
            Ride::STATUS_STOPPED,
            18,
            -23,
            45
        );
    }

    public function testFallsNegative()
    {
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage('Invalid number of falls, it cant be negative');

        $wagons = [$this->createMock(Wagon::class)];

        $obj = new WaterRide(
            'aName',
            $wagons,
            Ride::STATUS_STOPPED,
            18,
            23,
            -45
        );
    }
}