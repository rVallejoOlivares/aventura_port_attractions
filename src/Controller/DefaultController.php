<?php


namespace App\Controller;

use App\Entity\BigDrop;
use App\Entity\RollerCoaster;
use App\Entity\Wagon;
use App\Entity\WaterRide;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class DefaultController extends AbstractController
{
    public function indexAction()
    {
        $wagon = new Wagon(24, "Bidons de vi");

        $grape = new RollerCoaster (
            "Grape furia",
            [$wagon, $wagon],
            RollerCoaster::STATUS_STOPPED,
            0,
            135,
            0,
            2,
            "acer"
        );

        $wagon = new Wagon(28, "Drac");

        $dragon = new RollerCoaster(
            "Dragon rice",
            [$wagon, $wagon, $wagon],
            RollerCoaster::STATUS_STOPPED,
            8,
            105,
            50,
            3,
            "acer"
        );

        $wagon = new Wagon(24, "Vagonetes mineres");

        $bull = new RollerCoaster(
            "Bull race",
            [$wagon, $wagon, $wagon, $wagon],
            RollerCoaster::STATUS_STOPPED,
            0,
            74,
            27,
            4,
            "fusta"
        );

        $wagon = new Wagon(20, "Barques motores");

        $totem = new WaterRide (
            "Totem patachof",
            [$wagon, $wagon, $wagon, $wagon, $wagon, $wagon, $wagon, $wagon],
            WaterRide::STATUS_STOPPED,
            315,
            8,
            2
        );

        $wagon = new Wagon(5, "Troncs");

        $golden = new WaterRide (
            "Golden Torrent Duct",
            [$wagon, $wagon, $wagon, $wagon, $wagon, $wagon, $wagon, $wagon, $wagon, $wagon, $wagon, $wagon],
            WaterRide::STATUS_STOPPED,
            390,
            12,
            3
        );

        $wagon = new Wagon(5, "Àgila");

        $tornado = new BigDrop (
            "Tornado Eagle",
            [$wagon, $wagon, $wagon, $wagon],
            BigDrop::STATUS_STOPPED,
            100,
            115
        );

        echo '<h1> Aventura port </h1>';

        for ($i=1; $i <= 7; $i++) {
            echo '<div class="dayDiv">';
            echo '<h2 style="color: forestgreen;"> Day ' . $i . '</h2>';

            $grape->startUp();
            $dragon->startUp();
            $bull->startUp();
            $totem->startUp();
            $golden->startUp();
            $tornado->startUp();

            for ($x=10; $x <= 19; $x++) {
                echo '<div class="hourDiv">';
                echo '<h3 style="color: forestgreen; margin-top: 15px;"> Hour ' . $x . ':00</h3>';
                $dragon->removeWagon();
                if($x%2===0){
                    $dragon->addWagon();
                }
                $grape->addWagon();

                $grape->doLap();
                $dragon->doLap();
                $bull->doLap();
                $totem->doLap();
                $golden->doLap();
                $tornado->doLap();
                echo '</div><hr>';
            }

            $grape->shutDown();
            $dragon->shutDown();
            $bull->shutDown();
            $totem->shutDown();
            $golden->shutDown();
            $tornado->shutDown();

            echo '<br><h2> park closed</h2><hr>';
            echo '</div>';
        }

        return $this->render('index.html.twig');
    }
}