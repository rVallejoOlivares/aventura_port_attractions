<?php


namespace App\Entity;


use http\Exception\RuntimeException;

class WaterRide extends Ride
{

    /** @var int */
    private $duration;

    /** @var int */
    private $minWagons;

    /** @var int */
    private $falls;

    public function __construct(string $name, array $wagons, string $status, $duration, $minWagons, $falls)
    {
        parent::__construct($name, $wagons, $status);
        if($duration<=0) throw new \RuntimeException('Invalid duration, it cant be negative or zero');
        $this->duration = $duration;
        if($minWagons<=0) throw new \RuntimeException('Invalid minimum of wagons, it cant be negative or zero');
        $this->minWagons = $minWagons;
        if($falls<0) throw new \RuntimeException('Invalid number of falls, it cant be negative');
        $this->falls = $falls;
    }

    /**
     * @return int
     */
    public function Duration(): int
    {
        return $this->duration;
    }

    /**
     * @return int
     */
    public function MinWagons(): int
    {
        return $this->minWagons;
    }

    /**
     * @return int
     */
    public function Falls(): int
    {
        return $this->falls;
    }

    public function doLap()
    {
        if ($this->Status() === Ride::STATUS_STARTED) {
            echo '<span style="color: blue; font-size: 23px;">' . $this->Name() .'</span> starting with ' . $this->fillRide(). '/' . $this->getTotalSeats() . ' passengers, waggons: ' . count($this->Wagons()) . '<br>';
            echo 'going up...<br>';
            for ($i = 0; $i <= $this->falls; $i++){
                echo 'doing fall nº.' . $i .'<br>';
            }
            echo 'finish<br><br>';
        } else {
            echo $this->Name() . ' is stopped, start ride before do a lap<br><br>';
        }
    }

    use TraitRideWagons {
        removeWagon as public;
        addWagon as public;
    }

}