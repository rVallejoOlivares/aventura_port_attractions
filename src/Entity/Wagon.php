<?php

namespace App\Entity;


class Wagon
{
    /** @var int */
    private $ridersWagon;

    /** @var string */
    private $theme;

    /**
     * Wagon constructor.
     * @param int $ridersWagon
     * @param string $theme
     */
    public function __construct(int $ridersWagon, string $theme)
    {
        $this->ridersWagon = $ridersWagon;
        $this->theme = $theme;
    }

    /**
     * @return int
     */
    public function RidersWagon(): int
    {
        return $this->ridersWagon;
    }

    /**
     * @return string
     */
    public function Theme(): string
    {
        return $this->theme;
    }

}