<?php

namespace App\Entity;


class RollerCoaster extends Ride
{

    /** @var int */
    private $numLoops;

    /** @var int */
    private $maxSpeed;

    /** @var int */
    private $initialDropDistance;

    /** @var int */
    private $minWagons;

    /** @var string */
    private $material;


    public function __construct(string $name, array $wagons, string $status, int $numLoops, int $maxSpeed, int $initialDropDistance, int $minWagons, string $material)
    {
        parent::__construct($name, $wagons, $status);
        if ($numLoops<0) throw new \RuntimeException('Invalid number of loops, it cant be negative');
        $this->numLoops = $numLoops;
        if ($maxSpeed<=0) throw new \RuntimeException('Invalid max speed, it cant be negative or zero');
        $this->maxSpeed = $maxSpeed;
        if ($initialDropDistance<0) throw new \RuntimeException('Invalid initial drop distance, it cant be negative');
        $this->initialDropDistance = $initialDropDistance;
        if ($minWagons<=0) throw new \RuntimeException('Invalid minimum of wagons, it cant be negative or zero');
        $this->minWagons = $minWagons;
        $this->material = $material;
    }

    /**
     * @return int
     */
    public function MaxSpeed(): int
    {
        return $this->maxSpeed;
    }

    /**
     * @return int
     */
    public function NumLoops(): int
    {
        return $this->numLoops;
    }

    /**
     * @return int
     */
    public function InitialDropDistance(): int
    {
        return $this->initialDropDistance;
    }

    /**
     * @return int
     */
    public function MinWagons(): int
    {
        return $this->minWagons;
    }

    /**
     * @return string
     */
    public function Material(): string
    {
        return $this->material;
    }

    public function doLap()
    {
        if ($this->Status() === Ride::STATUS_STARTED) {
            echo '<span style="color: blue; font-size: 23px;">' . $this->Name() .'</span> starting with ' . $this->fillRide(). '/' . $this->getTotalSeats() . ' passengers, waggons: ' . count($this->Wagons()) . '<br>';
            if ($this->initialDropDistance !== 0) {
                echo 'going up<br>';
                echo 'falling down<br>';
            }
            for ($i = 0; $i < $this->numLoops; $i++) {
                echo 'doing loop nº.' . $i .'<br>';
            }
            echo 'doing lap, vel: ' . $this->MaxSpeed() . 'km/h<br>';
            echo 'finish<br><br>';
        } else {
            echo $this->Name() . ' is stopped, start ride before do a lap<br><br>';
        }
    }

    use TraitRideWagons {
        removeWagon as public;
        addWagon as public;
    }

}