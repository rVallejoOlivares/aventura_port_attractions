<?php


namespace App\Entity;

trait TraitRideWagons
{
    public function removeWagon()
    {
        $wagons = $this->Wagons();
        if (count($wagons) <= $this->minWagons) {
            echo 'Wagon not removed, the minimum wagons of ' . $this->Name() . ' is ' . $this->minWagons . '<br>';
        } else {
            array_pop($wagons);
            $this->wagons = $wagons;
            echo 'Wagon removed from ' . $this->Name() . '<br>';
        }

    }

    public function addWagon()
    {
        try {
            $wagons = $this->Wagons();
            $newWagon = clone $wagons[0];
            array_push($wagons, $newWagon);
            $this->wagons = $wagons;
            echo 'Wagon added to ' . $this->Name() . '<br>';
        } catch (\Exception $e) {
            throw new $e;
        }
    }
}