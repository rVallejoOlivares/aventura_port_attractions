<?php


namespace App\Entity;


use App\Exception\RideMinWagonsException;



abstract class Ride
{
    //status from Rides
    public const STATUS_STARTED = 'Started';
    public const STATUS_STOPPED = 'Stopped';
    public const STATUS_DOINGLAP = 'DoingLap';

    /** @var string */
    protected $name;

    /** @var Wagon [] */
    protected $wagons;

    /** @var string */
    protected $status;

    /**
     * Attraction constructor.
     * @param string $name
     * @param array $wagons
     * @param string $status
     */
    public function __construct(string $name, array $wagons, string $status)
    {
        $this->name = $name;
        if (empty($wagons)) throw new \RuntimeException('Invalid wagons, it cant be empty');
        $this->wagons = $wagons;
        if (!\in_array($status, [Ride::STATUS_STARTED, Ride::STATUS_STOPPED], true)) {
            throw new \RuntimeException('Invalid status');
        }
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function Name(): string
    {
        return $this->name;
    }

    /**
     * @return Wagon[]
     */
    public function Wagons(): array
    {
        return $this->wagons;
    }

    /**
     * @return string
     */
    public function Status(): string
    {
        return $this->status;
    }


    public function startUp()
    {

        if ($this->status === $this::STATUS_STOPPED) {
            $this->status = $this::STATUS_STARTED;
            $msg = $this->name . ' started! <br>';
        } else {
            $msg = $this->name . ' already started! <br>';
        }

        echo $msg;
    }

    public function shutDown()
    {

        if ($this->status === $this::STATUS_STARTED) {
            $this->status = $this::STATUS_STOPPED;
            $msg = $this->name . ' stopped! <br>';
        } else {
            $msg = $this->name . ' already stopped! <br>';
        }

        echo $msg;
    }

    public abstract function doLap();

    public function getTotalSeats()
    {
        return count($this->Wagons())*$this->Wagons()[0]->RidersWagon();
    }

    public function fillRide()
    {
        return rand(10,$this->getTotalSeats());
    }
}