<?php


namespace App\Entity;

use phpDocumentor\Reflection\Types\Parent_;

class BigDrop extends Ride
{
    /** @var int */
    private $dropDistance;

    /** @var int */
    private $maxSpeed;

    public function __construct(string $name, array $wagons, string $status, int $dropDistance, int $maxSpeed)
    {
        parent::__construct($name, $wagons, $status);
        if($dropDistance<0) throw new \RuntimeException('Invalid drop distance, it cant be negative');
        $this->dropDistance = $dropDistance;
        if($maxSpeed<0) throw new \RuntimeException('Invalid max speed, it cant be negative');
        $this->maxSpeed = $maxSpeed;
    }

    /**
     * @return int
     */
    public function DropDistance(): int
    {
        return $this->dropDistance;
    }

    /**
     * @return int
     */
    public function MaxSpeed(): int
    {
        return $this->maxSpeed;
    }

    public function doLap()
    {
        if ($this->Status() === Ride::STATUS_STARTED) {
            echo '<span style="color: blue; font-size: 23px;">' . $this->Name() .'</span> starting with ' . $this->fillRide(). '/' . $this->getTotalSeats() . ' passengers, waggons: ' . count($this->Wagons()) . '<br>';
            echo 'going up...<br>';
            echo 'falling down, vel: ' . $this->MaxSpeed() . 'km/h<br>';
            echo 'finish<br><br>';
        } else {
            echo $this->Name() . ' is stopped, start ride before do a lap<br><br>';
        }
    }

    public function startUp()
    {
        parent::startUp();

        if ($this->status === $this::STATUS_STOPPED) {
            $this->status = $this::STATUS_STARTED;
            $msg = $this->name . '\'s wagons going down and started! <br>';
        } else {
            $msg = $this->name . ' already started! <br>';
        }

        echo $msg;
    }

}