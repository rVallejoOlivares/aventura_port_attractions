<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 15/02/19
 * Time: 17:39
 */

namespace App\Core;


use Doctrine\DBAL\Connection;

class DoctrineManager
{
    /** @var Connection */
    private $connection;

    /**
     * DoctrineManager constructor.
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function queryBuilder()
    {
        return $this->connection->createQueryBuilder();
    }

    public function prepare($sql)
    {
        return $this->connection->prepare($sql);
    }


}