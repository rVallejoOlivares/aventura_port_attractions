<?php


namespace App\Exception;


use Throwable;

class AventuraException extends \Exception
{

    const MINIMUM_WAGONS = 001;

    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}