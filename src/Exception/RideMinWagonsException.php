<?php


namespace App\Exception;


class RideMinWagonsException extends \Exception
{
    public function __construct(\Throwable $previous = null)
    {
        parent::__construct('can\'t remove one, ride\'s wagons minimum reached', AventuraException::MINIMUM_WAGONS, $previous);
    }
}